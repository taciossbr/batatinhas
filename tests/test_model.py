from unittest import TestCase, mock
from app.models import Usuario

from os import rename, remove


class TesteModelUsuario(TestCase):

    # def setUp(self):
    #     remove('base.db')

    # def setUpClass(self):
    #     rename('base.db', '_base.db')

    # def tearDownClass(self):
    #     rename('_base.db', 'base.db')

    def test_table_name_should_return_usuario(self):
        esperado = 'Usuario'
        result = Usuario(nome='tacio', sobrenome='santos')
        self.assertEqual(esperado, result.__tablename__)

    def test_usuario_repr(self):
        esperado = 'Usuario(nome=\'tacio\', sobrenome=\'santos\')'
        result = Usuario(nome='tacio', sobrenome='santos')
        self.assertEqual(esperado, Usuario.__repr__(result))
