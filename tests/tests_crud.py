from unittest import TestCase, mock
from json import dumps

from flask import url_for

from app import app
from app.models import Usuario


class FirstTestCase(TestCase):
    def setUp(self):
        self.app = app
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.client = self.app.test_client()
        self.app.testing = True

    def test_root_should_return_200(self):
        result = self.client.get(url_for('crud.root'))

        self.assertEqual(200, result.status_code)

    @mock.patch('app.crud.session')
    def test_get_users(self, m_session):
        user = {'id': 1, 'nome': 'tacio', 'sobrenome': 'santos'}
        m_session.query(Usuario).all.return_value = [Usuario(**user)]
        # m_session.session.query.all.return_value = Usuario(**user)
        result = self.client.get(url_for('crud.get_users'))
        esperado = [user]

        self.assertEqual(esperado, result.json)

    @mock.patch('app.crud.session')
    def test_post_user(self, m_session):
        request_ob = {'nome': 'eduardo', 'sobrenome': 'mendes'}
        result = self.client.post(url_for('crud.post_users'), data=dumps(request_ob))

        self.assertTrue(m_session.add.called)
        # self.assertTrue(m_session.add.)
        # import ipdb; ipdb.set_trace()
        self.assertTrue(m_session.commit.called)
        self.assertEqual(result.data, b'ok')
