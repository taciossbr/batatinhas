from unittest import TestLoader, runner
import click

from app import app

@click.group()
def c():
    ...

@c.command()
def runserver():
    app.run(debug=True)

@c.command()
def tests():
    loader = TestLoader()
    tests = loader.discover("tests/")
    test_runner = runner.TextTestRunner()
    test_runner.run(tests)

c()