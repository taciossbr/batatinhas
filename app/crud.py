from flask import Blueprint, jsonify, request, render_template, make_response
from wtforms import Form, StringField, SubmitField
from .models import session, Usuario


crud_bd = Blueprint('crud', __name__)


class User(Form):
    nome = StringField('Nome')
    sobrenome = StringField('Sobrenome')
    but = SubmitField('OK!')


@crud_bd.route("/")
def root():
    return render_template('home.html', form=User())


@crud_bd.route("/get-users")
def get_users():
    users = [u.as_dict() for u in session.query(Usuario).all()]
    resp = make_response(jsonify(users))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    # import ipdb; ipdb.set_trace()
    return resp


@crud_bd.route("/post-user", methods=['POST'])
def post_users():
    user = request.get_json(force=True)
    # print(users)
    db_user = Usuario(**user)
    session.add(db_user)
    session.commit()
    return 'ok'
