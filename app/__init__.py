from flask import Flask
from .crud import crud_bd
from .models import session

app = Flask(__name__)

app.register_blueprint(crud_bd, url_prefix='/')


@app.teardown_request
def remove_session(ex=None):
    session.remove()
